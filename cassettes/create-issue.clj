{:calls [{:arg-key {:query-string nil,
                    :request-method :post,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/4202845/issues"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6NjkyMDA2MiwiaWlkIjo0LCJwcm9qZWN0X2lkIjo0MjAyODQ1LCJ0aXRsZSI6IldvdyB"
                    "tdWNoIGlzc3VlIiwiZGVzY3JpcHRpb24iOm51bGwsInN0YXRlIjoib3BlbmVkIiwiY3JlYXRlZF"
                    "9hdCI6IjIwMTctMDktMjJUMTQ6MTY6MjQuODk4WiIsInVwZGF0ZWRfYXQiOiIyMDE3LTA5LTIyV"
                    "DE0OjE2OjI0Ljg5OFoiLCJsYWJlbHMiOltdLCJtaWxlc3RvbmUiOm51bGwsImFzc2lnbmVlcyI6"
                    "W10sImF1dGhvciI6eyJpZCI6Mzg1MjcsIm5hbWUiOiJEb2dlIERldiIsInVzZXJuYW1lIjoiZG9"
                    "nZWRldiIsInN0YXRlIjoiYWN0aXZlIiwiYXZhdGFyX3VybCI6Imh0dHBzOi8vZ2l0bGFiLmNvbS"
                    "91cGxvYWRzLy0vc3lzdGVtL3VzZXIvYXZhdGFyLzM4NTI3L2RvZ2UuanBlZyIsIndlYl91cmwiO"
                    "iJodHRwczovL2dpdGxhYi5jb20vZG9nZWRldiJ9LCJhc3NpZ25lZSI6bnVsbCwidXNlcl9ub3Rl"
                    "c19jb3VudCI6MCwidXB2b3RlcyI6MCwiZG93bnZvdGVzIjowLCJkdWVfZGF0ZSI6bnVsbCwiY29"
                    "uZmlkZW50aWFsIjpmYWxzZSwid2VpZ2h0IjpudWxsLCJ3ZWJfdXJsIjoiaHR0cHM6Ly9naXRsYW"
                    "IuY29tL2RvZ2VkZXYvdGVzdC1jbGktZ2l0bGFiL2lzc3Vlcy80IiwidGltZV9zdGF0cyI6eyJ0a"
                    "W1lX2VzdGltYXRlIjowLCJ0b3RhbF90aW1lX3NwZW50IjowLCJodW1hbl90aW1lX2VzdGltYXRl"
                    "IjpudWxsLCJodW1hbl90b3RhbF90aW1lX3NwZW50IjpudWxsfSwiX2xpbmtzIjp7InNlbGYiOiJ"
                    "odHRwOi8vZ2l0bGFiLmNvbS9hcGkvdjQvcHJvamVjdHMvNDIwMjg0NS9pc3N1ZXMvNCIsIm5vdG"
                    "VzIjoiaHR0cDovL2dpdGxhYi5jb20vYXBpL3Y0L3Byb2plY3RzLzQyMDI4NDUvaXNzdWVzLzQvb"
                    "m90ZXMiLCJhd2FyZF9lbW9qaSI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80"
                    "MjAyODQ1L2lzc3Vlcy80L2F3YXJkX2Vtb2ppIiwicHJvamVjdCI6Imh0dHA6Ly9naXRsYWIuY29"
                    "tL2FwaS92NC9wcm9qZWN0cy80MjAyODQ1In0sInN1YnNjcmliZWQiOnRydWV9"],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "1002",
                    "Content-Type" "application/json",
                    "Date" "Fri, 22 Sep 2017 14:16:25 GMT",
                    "Etag" "W/\"640486c57224183669f697028e126b3d\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "1",
                    "RateLimit-Remaining" "599",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "ce246d29-bab7-481d-9a6b-4d8e0b92694a",
                    "X-Runtime" "0.730498"},
                   :length 1002,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "Created",
                   :repeatable? false,
                   :status 201,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-22T14:16:21.609-00:00"}
