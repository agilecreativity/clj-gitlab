(ns clj-gitlab.commits
  (:use [clj-gitlab.core :only [api-get]]))

(defn- repository-path [project-id]
  (str "/projects/" project-id "/repository"))

(defn commits [project-id & [options]]
  (api-get (str (repository-path project-id) "/commits") options))

(defn commit [project-id sha & [options]]
  (api-get (str (repository-path project-id) "/commits/" sha) options))

(defn commit-diff [project-id sha & [options]]
  (api-get (str (repository-path project-id) "/commits/" sha "/diff") options))

(defn commit-comments [project-id sha & [options]]
  (api-get (str (repository-path project-id) "/commits/" sha "/comments") options))
